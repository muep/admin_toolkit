#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# ca.py - set up a minimal openssl-based CA into a
# subdirectory. Also contains some nifty wrappers for
# easier openssl invocation.
#
# Copyright (c) 2014, Joonas Sarajärvi <muep@iki.fi>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#  Redistributions of source code must retain the above copyright
#  notice, this list of conditions and the following disclaimer.
#
#  Redistributions in binary form must reproduce the above copyright
#  notice, this list of conditions and the following disclaimer in the
#  documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import argparse
import os
import stat
import subprocess
import sys

openssl_conf_template = """
# Own keys

[ req ]

default_bits       = 1024
default_keyfile    = key.pem
distinguished_name = {name}_req
attributes         = {name}_attrs
x509_extensions    = v3_ca

[ {name}_req ]
countryName          = Country Name (2 letter code)
countryName_default  = FI

commonName           = Common Name (e.g. server FQDN or YOUR name)
commonName_default   = ca.{name}

[ {name}_attrs ]
challengePassword               = A challenge password
challengePassword_min           = 4
challengePassword_max           = 20

[ v3_ca ]
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid:always,issuer
basicConstraints = CA:true

# CA
[ ca ]
default_ca = ca_{name}

[ ca_{name} ]

dir              = {ca_root}
certs            = $dir/certs
database         = $dir/index.txt
new_certs_dir    = $dir/newcerts
certificate      = $dir/ca.{name}.cert.pem
serial           = $dir/serial

private_key      = $dir/private/ca.{name}.key.pem
RANDFILE         = $dir/private/.rand

default_days     = 365
default_crl_days = 30
default_md       = sha1

policy           = {name}_policy

[ {name}_policy ]
countryName            = supplied
stateOrProvinceName    = optional
organizationName       = optional
organizationalUnitName = optional
commonName             = supplied
emailAddress           = optional
"""

def filepaths_under_dir(name=None, path=None):
    d = {
        "ca_root": path,
        "ca_certs": path + "/certs",
        "ca_private": path + "/private",
        "ca_newcerts": path + "/newcerts",
        "serial": path + "/serial",
        "index": path + "/index.txt",
        "ca_key": path + "/private/ca.{}.key.pem".format(name),
        "ca_cert": path + "/ca.{}.cert.pem".format(name),
        "openssl_conf" : path + "/openssl.cnf",
        }
    return d

def perform_init(name = None, path = None):
    paths = filepaths_under_dir(name=name, path=path)

    # flags for the chown command
    mode_700 = stat.S_IRUSR | stat.S_IWUSR | stat.S_IXUSR

    if os.path.exists(paths["ca_root"]):
        raise Exception("path '{}' exists".format(paths["ca_root"]))

    print("Initializing {} to '{}'...".format(name, paths["ca_root"]))
    print("Creating {}".format(paths["ca_root"]))
    os.makedirs(paths["ca_root"])
    os.chmod(paths["ca_root"], mode_700)
    for p in ("ca_certs", "ca_private", "ca_newcerts"):
        os.makedirs(paths[p])

    with open(paths["serial"], "w") as f:
        print("1000", file=f)

    with open(paths["index"], "w") as f:
        # Just need to make an empty file here.
        pass

    print("Generating '{}'".format(paths["openssl_conf"]))
    with open(paths["openssl_conf"], "w") as f:
        print(openssl_conf_template.format(
                name=name,
                ca_root=paths["ca_root"]),
            file=f)

    openssl_req_new = [
        "openssl", "req", "-new", "-x509",
        "-days", "3650",
        "-keyout", paths["ca_key"],
        "-out", paths["ca_cert"],
        "-config", paths["openssl_conf"]
        ]
    subprocess.call(openssl_req_new)

def perform_newkey(name=None, path=None):
    key_path = path + "/{}.key.pem".format(name)
    csr_path = path + "/{}.csr.pem".format(name)

    if os.path.exists(key_path):
        raise Exception("{} exists already".format(key_path))
    if os.path.exists(csr_path):
        raise Exception("{} exists already".format(csr_path))

    os.makedirs(path)

    openssl_req_new = [
        "openssl", "req", "-new",
        "-newkey", "rsa:2048",
        "-nodes",
        "-keyout", key_path,
        "-out", csr_path,
        ]
    subprocess.call(openssl_req_new)

def perform_certify(ca_path, csr_path, cert_path):
    if os.path.exists(cert_path):
        raise Exception("{} exists already".format(cert_path))

    paths = filepaths_under_dir(name="unknown", path=ca_path)

    openssl_ca = [
        "openssl", "ca",
        "-config", paths["openssl_conf"],
        "-out", cert_path,
        "-infiles", csr_path,
        ]

    subprocess.call(openssl_ca)

def output_from_success(cmd):
    try:
        return subprocess.check_output(cmd, stderr=subprocess.STDOUT)
    except subprocess.CalledProcessError:
        return None

def perform_show(file_path=None):
    show_key = [
        "openssl", "pkey",
        "-noout", "-text",
        "-in", file_path
        ]

    show_csr = [
        "openssl", "req",
        "-noout", "-text",
        "-in", file_path
        ]

    show_cert = [
        "openssl", "x509",
        "-noout", "-text",
        "-in", file_path
        ]

    output = output_from_success(show_csr)
    if output:
        print(output.decode())
        return
    output = output_from_success(show_cert)
    if output:
        print(output.decode())
        return
    output = output_from_success(show_key)
    if output:
        print(output.decode())
        return
    print("No idea about what I should show")

help_init = "set up a certificate authority in a subdirectory"
def setup_init_argumentparser(p):
    name_help = "Name of the Certificate Authority (CA). "\
        "Currently this works so that "\
        "passing foo here will result in a CA whose Common Name "\
        "is ca.foo. This is possibly a bad choice and may "\
        "be revised at some point."
    p.add_argument("name", help=name_help)

    path_help = "Path where to create the Certificate Authority. "\
        "This must not exist beforehand."
    p.add_argument("path", help=path_help)

help_newkey = "generate a new key, CSR pair"
def setup_newkey_argumentparser(p):
    p.add_argument("name")
    p.add_argument("path")

help_certify = "apply a CA from init-ca to fulfill a CSR"
def setup_certify_argumentparser(p):
    p.add_argument("ca_path")
    p.add_argument("csr_path")
    p.add_argument("cert_path")

help_show = "display details of some artifact"
def setup_show_argumentparser(p):
    p.help = "lol"
    p.add_argument("file_path", metavar="file-path")

def setup_main_argumentparser(p):
    subs = p.add_subparsers(dest="subcmd")

    init_parser = subs.add_parser("init-ca", help=help_init)
    setup_init_argumentparser(init_parser)

    nk_parser = subs.add_parser("new-key", help=help_newkey)
    setup_newkey_argumentparser(nk_parser)

    certify_parser = subs.add_parser("certify", help=help_certify)
    setup_certify_argumentparser(certify_parser)

    show_parser = subs.add_parser("show", help=help_show)
    setup_show_argumentparser(show_parser)

def main(argv):
    p = argparse.ArgumentParser()
    setup_main_argumentparser(p)

    parsed_args = p.parse_args(argv[1:])

    if parsed_args.subcmd == "init-ca":
        perform_init(path=parsed_args.path, name=parsed_args.name)
    elif parsed_args.subcmd == "new-key":
        perform_newkey(path=parsed_args.path, name=parsed_args.name)
    elif parsed_args.subcmd == "show":
        perform_show(file_path=parsed_args.file_path)
    elif parsed_args.subcmd == "certify":
        perform_certify(ca_path=parsed_args.ca_path,
                        csr_path=parsed_args.csr_path,
                        cert_path=parsed_args.cert_path)
    return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv))
