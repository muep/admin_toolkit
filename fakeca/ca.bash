#!/bin/bash
#
# ca.bash
#
# A possibly questionable tool for setting up a self-contained
# OpenSSL based x.509 certificate authority into a directory.

if test $# -ne 2
then
        echo "usage ca.bash <name> <path>"
        exit 1
fi

name="$1"
if test -z "${name}"
then
        echo name was empty
        exit 3
fi

CA="$2"
if test -e "${CA}"
then
        echo "${CA}" already exists
        exit 2
fi

SSL_CNF="${CA}/openssl.cnf"

echo "Creating the directory layout..."
mkdir -pv ${CA}/{certs,private,newcerts}
chmod 700 ${CA}
echo 1000 > ${CA}/serial
touch ${CA}/index.txt

echo "Generating ${SSL_CNF}"
cat > ${SSL_CNF} << EOF

# Own keys

[ req ]

default_bits       = 1024
default_keyfile    = key.pem
distinguished_name = ${name}_req
attributes         = ${name}_attrs
x509_extensions    = v3_ca

[ ${name}_req ]
countryName          = Country Name (2 letter code)
countryName_default  = FI

commonName           = Common Name (e.g. server FQDN or YOUR name)
commonName_default   = ca.${name}

[ ${name}_attrs ]
challengePassword               = A challenge password
challengePassword_min           = 4
challengePassword_max           = 20

[ v3_ca ]
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid:always,issuer
basicConstraints = CA:true

# CA
[ ca ]
default_ca = ca_${name}

[ ca_${name} ]

dir              = ${CA}
certs            = \$dir/certs
database         = \$dir/index.txt
new_certs_dir    = \$dir/newcerts
certificate      = \$dir/ca.${name}.cert.pem
serial           = \$dir/serial

private_key      = \$dir/private/ca.${name}.key.pem
RANDFILE         = \$dir/private/.rand

default_days     = 365
default_crl_days = 30
default_md       = sha1

policy           = ${name}_policy

[ ${name}_policy ]
countryName            = supplied
stateOrProvinceName    = optional
organizationName       = optional
organizationalUnitName = optional
commonName             = supplied
emailAddress           = optional
EOF

echo "Setting up CA keys..."

# Valid for almost 10 years
openssl req -new -x509 -days 3650 \
 -keyout "${CA}/private/ca.${name}.key.pem" \
 -out ${CA}/ca.${name}.cert.pem \
 -config "${SSL_CNF}"
