#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# backup.py - a small thing that adds a configuration facility and
# other conveniences on top of duplicity.
#
# Copyright (c) 2013, Joonas Sarajärvi <muep@iki.fi>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#  Redistributions of source code must retain the above copyright
#  notice, this list of conditions and the following disclaimer.
#
#  Redistributions in binary form must reproduce the above copyright
#  notice, this list of conditions and the following disclaimer in the
#  documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

# The script is intended to be fully bilingual between Python 2.7 and
# Python 3.3. Thus all this future stuff.
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import argparse
import os
import shlex
import subprocess
import time

class ConfigError(Exception): pass

try:
    # Python 3
    import configparser as cp
except ImportError:
    # Python 2
    import ConfigParser as cp

def arg_parser():
    p = argparse.ArgumentParser()
    p.add_argument("--config", default="/etc/backup.conf")
    p.add_argument("--allow-source-mismatch",
                   action="store_true",
                   help="pass --allow-source-mismatch to duplicity")
    p.add_argument("--show-rc",
                   action="store_true",
                   help="Just print out the runtime configuration")
    return p

def unpack_excludes(exclude_txt):
    def add_option(item):
        return "--exclude={}".format(item)

    return list(map(add_option, exclude_txt.split()))

# Helper for read_config that reads the
# pre_cmd option and lexes it if possible.
def read_precmd(p, section):
    try:
        txt = p.get(section, "pre_cmd")
    except Exception:
        return None

    try:
        return shlex.split(txt)
    except Exception:
        msg = "Invalid syntax in pre_cmd of {} section".format(section)
        raise ConfigError(msg)

def read_config(parsed_args):
    configpath = parsed_args.config
    p = cp.ConfigParser()

    p.read([configpath])

    rcs = []

    for section in p.sections():
        try:
            passphrase = p.get(section, "passphrase")
        except Exception:
            passphrase = None

        try:
            ssh_id_rsa = p.get(section, "ssh_id_rsa")
        except Exception:
            ssh_id_rsa = None

        try:
            max_chain_age = p.get(section, "max_chain_age")
        except Exception:
            max_chain_age = None

        pre_cmd = read_precmd(p, section)

        rc = {
            "name" : section,
            "source" : p.get(section, "source"),
            "destination" : p.get(section, "destination"),
            "exclude" : unpack_excludes(p.get(section, "exclude")),
            "passphrase" : passphrase,
            "ssh_id_rsa" : ssh_id_rsa,
            "pre_cmd" : pre_cmd,
            "max_chain_age" : max_chain_age,
            "allow_source_mismatch" : parsed_args.allow_source_mismatch
            }
        rcs.append(rc)

    return rcs

def duplicity_params(rc):
    l = ["duplicity",
         rc["source"], rc["destination"]]

    max_chain_age = rc["max_chain_age"]
    if max_chain_age != None:
        l.append("--full-if-older-than={}".format(max_chain_age))

    ssh_id_rsa = rc["ssh_id_rsa"]
    if ssh_id_rsa != None:
        l.append("--ssh-options=-oIdentityFile={}".format(ssh_id_rsa))

    if rc["passphrase"] == None:
        l.append("--no-encryption")

    if rc["allow_source_mismatch"]:
        l.append("--allow-source-mismatch")

    l += rc["exclude"]

    return l

def run(rc):
    dp = duplicity_params(rc)
    prep = rc["pre_cmd"]

    if prep != None:
        subprocess.check_call(prep, stdin=None)

    env = {}
    for key in os.environ:
        env[key] = os.environ[key]

    if rc["passphrase"] != None:
        env["PASSPHRASE"] = rc["passphrase"]

    subprocess.check_call(dp, env=env,
                          stdin=None)


def run_until_success(rc):
    name = rc["name"]
    fail_wait = 600
    while True:
        try:
            print("Taking backups of {0}:".format(name))
            run(rc)
            return
        except Exception as e:
            print("Failed to take backups of {0}: {1}".format(name, e))
            print("Sleeping for {0} seconds.".format(fail_wait))
            time.sleep(fail_wait)


def show_rc(rc):
    print("configuration of [{}]".format(rc["name"]))
    for key in sorted(filter(lambda k: k != "name", rc)):
        print("{:>15}: {}".format(key,rc[key]))

def main():
    pargs = arg_parser().parse_args()

    rcs = read_config(pargs)

    if pargs.show_rc:
        for rc in rcs:
            show_rc(rc)
            print()
        return

    for rc in rcs:
        run_until_success(rc)

if __name__ == "__main__":
    main()
