#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# snapshot.py - A tool for taking snapshots of btrfs subvolumes
# in a system that uses a specific layout.
#
# Copyright (c) 2013, Joonas Sarajärvi <muep@iki.fi>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#  Redistributions of source code must retain the above copyright
#  notice, this list of conditions and the following disclaimer.
#
#  Redistributions in binary form must reproduce the above copyright
#  notice, this list of conditions and the following disclaimer in the
#  documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

# Useful information on anyone planning to use this script:
#
# The expected filesystem layout for this script is such, that
# the subvolume with id 0 is is mounted into /btrfs. Under it,
# should be at least these subvolumes:
#
# /btrfs/home - likely mounted to /home but could be elsewhere
# /btrfs/root - the subvolume that is mounted into /
# /btrfs/opt  - likely mounted to /opt but could be elsewhere
#
# Then for storing the snapshots, there should be at least these
# directories:
# /btrfs/snapshots/home
# /btrfs/snapshots/root
# /btrfs/snapshots/opt
#
# Notes about Fedora, Rsync and SELinux:
#
# The default "targeted" SELinux policy in Fedora targets rsync
# so that it can not read and write just anywhere it pleases. These
# adjustments should be done in current Fedora systems to make the
# rsync section below work:
#
# 1. Allow reading from /boot
#   setsebool -P rsync_export_all_ro on
#
# 2. Allow writes to public_content_rw_t
#  setsebool -P rsync_anon_write on
#
# 3. Mark /nonbtrfs as public_content_rw_t
#  semanage fcontext -a -t public_content_rw_t '/nonbtrfs(/.*)?'

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import argparse
import os
import os.path
import subprocess
import sys
import time

# Initial part of a btrfs subvol snapshot invocation
btrfs_ss = ["btrfs", "subvolume", "snapshot", "-r"]

class PrecheckError(Exception):
    pass

def precheck_operation(op):
    source = op["source"]
    dest = op["destination"]
    dest_parent = os.path.dirname(dest)

    if not os.path.isdir(source):
        msg = "Source {} can not be snapshotted".format(source)
        raise PrecheckError(msg)

    if os.path.exists(dest):
        msg = "Destination {} exists".format(dest)
        raise PrecheckError(msg)

    if not os.path.isdir(dest_parent):
        msg = "Directory {} does not exist".format(dest_parent)
        raise PrecheckError(msg)

def stamp_text():
    # Stamping according to local time is a bit questionable.

    # The empty string will ensure that we return
    # unicode text on python 2.
    return "" + time.strftime("%Y-%m-%dT%H%M%S",time.localtime())

def update_nonbtrfs(pretend):
    if pretend:
        print("would have copied /boot to /nonbtrfs/boot")
        return

    subprocess.check_call(["mkdir", "-p", "/nonbtrfs"])
    subprocess.check_call(["rsync",
                           "-avh", "--delete-during",
                           "/boot/", "/nonbtrfs/boot/"])

def perform_snapshot_op(op, pretend):
    src = op["source"]
    dst = op["destination"]

    if pretend:
        print("would have snapshotted {} to {}".format(src, dst))
        return

    subprocess.check_call(btrfs_ss + [src, dst])

def argument_parser():
    p = argparse.ArgumentParser(
        description="take btrfs snapshots"
    )

    p.add_argument("--verbose", action="store_true")
    p.add_argument("--pretend", action="store_true")

    p.add_argument("title")
    p.add_argument(
        "parts",
        choices=["all", "home", "sys"],
        metavar="parts",
        help="what to snapshot (one of all, home or sys)"
    )

    return p

def main(args):
    parser = argument_parser()
    parsed_args = parser.parse_args(args[1:])

    full_name = stamp_text() + "_" + parsed_args.title

    ops = []

    if parsed_args.parts in {"all", "home"}:
        src = "/btrfs/home"
        dst = "/btrfs/snapshots/home/{}".format(full_name)

        ops.append({
            "source": src,
            "destination": dst
        })

    if parsed_args.parts in {"all", "sys"}:
        for subvol in ["opt", "root"]:
            src = "/btrfs/" + subvol
            dst = "/btrfs/snapshots/{}/{}".format(subvol, full_name)
            ops.append({
                "source": src,
                "destination": dst
            })

    if parsed_args.verbose:
        print("Planned operations:")
        for op in ops:
            print("  {}".format(op))
        print("Checking for reasons not to perform these...")

    # Do some prechecking, to reduce the likelihood of failing
    # in the middle of taking snapshots.
    for op in ops:
        try:
            precheck_operation(op)
        except PrecheckError as e:
            src = op["source"]
            print("Will not be able to snapshot {}: {}".format(src, e))
            return 1

    if not parsed_args.pretend:
        if os.geteuid() != 0:
            print("Should be root in order to actually take snapshots")
            return 2

    if parsed_args.verbose:
        print("No reasons found.")
        print("Beginning snapshot operations")

    if parsed_args.parts in {"all", "sys"}:
        update_nonbtrfs(parsed_args.pretend)

    for op in ops:
        perform_snapshot_op(op, parsed_args.pretend)

if __name__ == "__main__":
    sys.exit(main(sys.argv))
