#!/usr/bin/env python
# coding=utf-8
#
# rrdrun.py - Some high-level functionality tacked on top of
# rrdtool

# Copyright (c) 2015, Joonas Sarajärvi <muep@iki.fi>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#  Redistributions of source code must retain the above copyright
#  notice, this list of conditions and the following disclaimer.
#
#  Redistributions in binary form must reproduce the above copyright
#  notice, this list of conditions and the following disclaimer in the
#  documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

# The script is intended to be fully bilingual between
# Python 2.7 and Python 3.
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import argparse
import json
import os
import subprocess
import sys
import time

def sources_from_layout(layout):
    def name_of_source(thing):
        ds_prefix = "DS:"

        if not thing.startswith(ds_prefix):
            return None

        prefix_len = len(ds_prefix)
        next_colon = thing[prefix_len:].find(":")
        if next_colon < 0:
            return None

        return thing[prefix_len:prefix_len + next_colon]

    return list(filter(lambda n: n != None,
                       map(name_of_source, layout)))

def rrd_create(rrdfile, layout):
    print("creating {}".format(rrdfile))
    cmd = ["rrdtool", "create", rrdfile] + layout
    subprocess.call(cmd)

def rrd_graph(rrdfile, imgfile, duration, items):
    hours = 6

    items_with_file = [item.format(rrdfile=rrdfile) for item in items]

    rrdtool = [
        "rrdtool", "graph",
        imgfile,
        "--start", "-{}".format(duration)
        ] + items_with_file
    subprocess.call(rrdtool)

def rrd_update(rrdfile, items):
    rrdtool = ["rrdtool", "update", rrdfile] + items
    print("running {}".format(rrdtool))
    subprocess.call(rrdtool)

def snmp_get(host, community, obj):
    def value_from_line(line):
        pos = line.rfind(" ")
        if pos < 0:
            raise Exception("expected space")

        return line[pos + 1:]

    snmpget = ["snmpget", "-v1", "-c", community, host, obj]

    lines = subprocess.check_output(snmpget).decode("UTF-8").split("\n")

    return value_from_line(lines[0])

def item_snmp(item):
    data = item.get("snmp-data", None)
    if data == None:
        return None

    sources = sources_from_layout(item["layout"])

    if set(sources) > set(data.keys()):
        print("snmp-data is missing some keys")
        return None

    update = [str(int(time.time()))]

    for source in sources:
        data_item = data[source]
        snmp_host = data_item["snmp-host"]
        snmp_obj = data_item["snmp-obj"]
        snmp_community = data_item.get("snmp-community", "public")

        update.append(snmp_get(snmp_host, snmp_community, snmp_obj))

    return ":".join(update)

def item_once(item):
    rrdfile = item["rrdfile"]

    if not os.path.exists(rrdfile):
        rrd_create(rrdfile, item["layout"])

    if "snmp-data" in item:
        snmp_value = item_snmp(item)
        rrd_update(rrdfile, [snmp_value])

    for graph in item["graphs"]:
        rrd_graph(rrdfile, graph["imgfile"],
                  graph["duration"], graph["items"])

def main_loop_once(config):
    for item in config:
        item_once(item)

def main_loop(config):
    while True:
        try:
            main_loop_once(config)
            time.sleep(10)
        except KeyboardInterrupt:
            return
        except Exception as e:
            print("Unexpected exception: {}".format(e))
            time.sleep(60)

def read_config(config_path):
    with open(config_path, "r") as f:
        return json.load(f)

def print_config(config):
    json.dump(config, sys.stdout, indent=4, sort_keys=True)

def argument_parser():
    p = argparse.ArgumentParser()
    p.add_argument("config_path")
    p.add_argument("--print-config", action="store_true")
    return p

def main():
    parser = argument_parser()
    pargs = parser.parse_args()

    config = read_config(pargs.config_path)
    if pargs.print_config:
        print_config(config)
        return

    main_loop(config)

if __name__ == "__main__":
    main()
