#!/usr/bin/env python
# coding=utf-8
#
# kickstart_maker.py - a simple-ish script for generating a kickstart
# file for automated installations of
# - CentOS 7
# - Fedora 19 and newer.
#
# Copyright (c) 2013-2015, Joonas Sarajärvi <muep@iki.fi>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#  Redistributions of source code must retain the above copyright
#  notice, this list of conditions and the following disclaimer.
#
#  Redistributions in binary form must reproduce the above copyright
#  notice, this list of conditions and the following disclaimer in the
#  documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.


# The script is intended to be fully bilingual between Python 2.7 and
# Python 3.
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals


import argparse
import crypt
import getpass
import sys

epel_repo = "repo --name=EPEL --baseurl=https://dl.fedoraproject.org/pub/epel/7/x86_64"
updates_repo = "repo --name=updates"

ext4_partitions = """
part / --fstype ext4 --size 500 --grow
"""


btrfs_partitions = """
part /boot --fstype=ext4 --size=250

part btrfs.01 --size 500 --grow

btrfs /btrfs --label main_btrfs btrfs.01
btrfs /                --subvol --name=root main_btrfs
btrfs /home            --subvol --name=home main_btrfs
btrfs /work            --subvol --name=work main_btrfs
btrfs /opt             --subvol --name=opt  main_btrfs

"""

salt_post_template = """
%post
{set_master}
{set_minion_id}
%end
"""

mirrorlist_urls = {
    "centos" : "http://mirrorlist.centos.org/?release=$releasever&arch=$basearch&repo=os&infra=$infra",
    "fedora" : "https://mirrors.fedoraproject.org/metalink?repo=fedora-$releasever&arch=$basearch",
    }

ks_template = """
text
reboot

url --mirrorlist={mirrorlist_url}

{repolist}

# System authorization information
auth --enableshadow --passalgo=sha512
rootpw --iscrypted {root_password}

# No firstboot, but set some defaults
firstboot --disable

timezone Europe/Helsinki --isUtc
keyboard --vckeymap=fi-latin1 --xlayouts='fi'
lang fi_FI.UTF-8

network  --bootproto=dhcp --hostname={hostname}
firewall --enabled --service=ssh

{salt_service}

# Partitioning
clearpart --all --initlabel

part swap --size=512

{fstype_specific_partitions}

bootloader --location=mbr

%packages
@core

{pkglist}

%end

{salt_post}

"""


def ks_text(params):
    if params["rootfs_type"] == "btrfs":
        part_txt = btrfs_partitions
    else:
        part_txt = ext4_partitions

    pkglist = "\n".join(params["packages"])
    repolist = "\n".join(params["extra_repos"])

    salt_master = params["salt_master"]
    salt_minion_id = params["salt_minion_id"]

    if salt_master is None:
        set_master = ""
    else:
        set_master = "echo 'master: {}' > /etc/salt/minion.d/master.conf".format(salt_master)

    if salt_minion_id is None:
        set_minion_id = ""
    else:
        set_minion_id = "echo '{}' > /etc/salt/minion_id".format(salt_minion_id)

    if (salt_master, salt_minion_id) != (None, None):
        salt_post = salt_post_template.format(set_master=set_master,
                                              set_minion_id=set_minion_id)
        salt_service = "services --enabled=salt-minion"
    else:
        salt_post = ""
        salt_service = ""

    return ks_template.format(
        fstype_specific_partitions=part_txt,
        root_password=params["root_password"],
        hostname=params["hostname"],
        pkglist=pkglist,
        mirrorlist_url=mirrorlist_urls[params["distribution"]],
        repolist=repolist,
        salt_post=salt_post,
        salt_service=salt_service,
    )


def argument_parser():
    distributions = [
        "centos",
        "fedora",
        ]

    p = argparse.ArgumentParser()

    # Using subcommands from the start, even if there is currently
    # just one command. There are some plans to add more.
    subs = p.add_subparsers(dest="cmd")

    cre_p = subs.add_parser("create")

    cre_p.add_argument("distribution",
                       choices=distributions,
                       help="target distribution")

    cre_p.add_argument("hostname")
    cre_p.add_argument("--rootfs-type",
                       choices=["ext4", "btrfs"],
                       default="ext4")
    cre_p.add_argument("--salt-minion-id",
                       help="minion id to use with salt master",
                       dest="salt_minion_id")
    cre_p.add_argument("--salt-master",
                       help="address of salt master",
                       dest="salt_master")
    cre_p.add_argument("--with-updates",
                       action="store_true",
                       help="use updates repository during installation")
    cre_p.add_argument("pkgs", nargs="*")

    return p


class PasswordPromptError(Exception):
    pass

# A function that simply prompts for a password and then runs it
# through crypt.crypt() to get a hash that can be used in the
# kickstart file.
def prompt_pw():
    pw0 = getpass.getpass("root password: ")
    pw1 = getpass.getpass("retype: ")

    if pw0 != pw1:
        raise PasswordPromptError("The provided passwords differ")

    return crypt.crypt(pw0)


def main(args):
    ap = argument_parser()

    parsed_args = ap.parse_args(args[1:])

    if parsed_args.cmd == None:
        print("Please remember to give a subcommand name",
              file=sys.stderr)
        return 1
    elif parsed_args.cmd == "create":

        root_password = prompt_pw()

        extra_repos = set()
        packages = set(parsed_args.pkgs)

        salt_minion_id = parsed_args.salt_minion_id
        salt_master = parsed_args.salt_master

        if salt_minion_id is not None or salt_master is not None:
            # If salt parameters were given, automatically
            # ensure that salt-minion gets installed.
            packages.add("salt-minion")

        # On CentOS, salt-minion installation requires
        # use of EPEL.
        if parsed_args.distribution == "centos":
            if "salt-minion" in packages:
                packages.add("epel-release")

            if "epel-release" in packages:
                extra_repos.add(epel_repo)

        if parsed_args.with_updates:
            extra_repos.add(updates_repo)

        params = {
            "distribution": parsed_args.distribution,
            "extra_repos": extra_repos,
            "rootfs_type" : parsed_args.rootfs_type,
            "root_password" : root_password,
            "hostname" : parsed_args.hostname,
            "packages" : packages,
            "salt_master": salt_master,
            "salt_minion_id": salt_minion_id,
        }

        print(ks_text(params))

    return 0


if __name__ == "__main__":
    sys.exit(main(sys.argv))
